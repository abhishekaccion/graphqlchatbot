import { EventEmitter as events } from "events";
const fetch = require("node-fetch");

export class Rest extends events {
  constructor() {
    super();
    this.maxRequest = 3;
    this.requestCount = 0;
    this.tokenRefreshed = false;
  }

  get(url, headers, body = {}) {
    return fetch(url, { method: "GET", headers: headers })
      .then(body => {
        return body;
      })
      .catch(error => {
        console.log(error);
        throw new Error(error.message);
      });
  }

  post(url, headers, body = {}) {
   
    return fetch(url, {
      method: "POST",
      headers: headers,
      body: JSON.stringify(body)
    })
      .then(res => res.json())
      .then(res => res)
      .catch(error => {
        console.log(error);
        throw new Error(error.message);
      });
  }
}
