import { GraphQLSchema, GraphQLObjectType } from "graphql";

import { userQueries, userMutations } from "./users/users";

import { chatQueries, chatMutations } from "./chat/chat";

export default new GraphQLSchema({
  query: new GraphQLObjectType({
    name: "Query",
    fields: () => ({
      ...userQueries,
      ...chatQueries
    })
  }),
  mutation: new GraphQLObjectType({
    name: "Mutation",
    fields: () => ({
      ...chatMutations
    })
  })
});
