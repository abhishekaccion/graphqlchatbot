import { GraphQLList, GraphQLNonNull, GraphQLString } from "graphql";
import { internet, random } from "faker";
import isEmail from "validator/lib/isEmail";
import { Rest } from "../../service/Rest";
import config from "../../config/config";

import { ChatType, ChatInputType, ChatTypeResponse } from "./chatTypes";

const chatQueries = {
  chat: {
    type: new GraphQLList(ChatType),
    args: {
      intent: {
        type: new GraphQLNonNull(GraphQLString),
        description: "intent name"
      }
    },
    resolve: async (_, { intent }) => {
      let rest = new Rest();
      let url =
        config.elasticSearchUrl +
        "/" +
        config.elasticSearchIndexName +
        "/chat/_search";
      let headers = config.header;
      let body = {
        query: {
          query_string: {
            query: intent,
            fields: ["intent"]
          }
        }
      };
      const result = await rest.post(url, headers, body);
      let tmpResult;

      if (result && result.hits && result.hits.hits[0]) {
        tmpResult = result.hits.hits[0]._source;
        tmpResult.id = result.hits.hits[0]._id;
      }
      let finalResult = [];
      finalResult.push(tmpResult);
      return finalResult;
    }
  }
};

const chatMutations = {
  createChat: {
    type: ChatTypeResponse,
    args: {
      input: {
        type: new GraphQLNonNull(ChatInputType)
      }
    },
    resolve: async (_, { input }) => {
      let rest = new Rest();
      let url =
        config.elasticSearchUrl +
        "/" +
        config.elasticSearchIndexName +
        "/chat/";
      let headers = config.header;

      const result = await rest.post(url, headers, input);
      return result;
    }
  }
};

export { chatQueries, chatMutations };
