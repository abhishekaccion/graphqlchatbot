import {
  GraphQLString,
  GraphQLID,
  GraphQLInputObjectType,
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLList
} from "graphql";

const ChatType = new GraphQLObjectType({
  name: "ChatType",
  description: "Chat type definition",
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    intent: {
      type: new GraphQLNonNull(GraphQLString)
    },
    response: {
      type: new GraphQLObjectType({
        name: "response",
        description: "intent response",

        fields: () => ({
          speech: {
            type: GraphQLString
          },
          data: {
            type: new GraphQLList(GraphQLString)
          }
        })
      }),
      description: "responnse"
    },
    type: {
      type: GraphQLString
    }
  })
});

const ChatTypeResponse = new GraphQLObjectType({
  name: "ChatTypeResponse",
  description: "Chat type definition",
  fields: () => ({
    _id: {
      type: GraphQLString
    }
  })
});

const ChatInputType = new GraphQLInputObjectType({
  name: "ChatInputType",
  description: "Chat type definition",
  fields: () => ({
    intent: {
      type: new GraphQLNonNull(GraphQLString)
    },
    response: {
      type: new GraphQLInputObjectType({
        name: "responseInput",
        description: "intent response",

        fields: () => ({
          speech: {
            type: GraphQLString
          },
          data: {
            type: new GraphQLList(GraphQLString)
          }
        })
      }),
      description: "responnse"
    },
    type: {
      type: GraphQLString
    }
  })
});
export { ChatType, ChatInputType, ChatTypeResponse };
