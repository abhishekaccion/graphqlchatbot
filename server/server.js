import express from "express";
import graphqlHTTP from "express-graphql";
import schema from "./graphql/schema";
import { Rest } from "../server/service/Rest";
import config from "../server/config/config";
import { request } from "http";

const bodyParser = require("body-parser");
const app = express();

//SSML innitialisation
var Speech = require("ssml-builder");

//const dev = process.env.NODE_ENV === "development";

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
  //allow cross origin requests
  res.setHeader(
    "Access-Control-Allow-Methods",
    "POST, PUT, OPTIONS, DELETE, GET"
  );
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, x-access-token, x-authpa-token"
  );
  res.header("Access-Control-Allow-Credentials", true);
  if (req.method === "OPTIONS") {
    res.status(200).send();
  } else {
    next();
  }
});

app.use(
  "/graphql",
  graphqlHTTP({
    schema,
    graphiql: true
  })
);

app.post("/webhook", async (req, res) => {
  var speech = new Speech();
  let ssmlSpeech = "";
  let args;
  let intent;
  let resolvedQuery;
  let requestClient = "dialogflow";
  let finalResponse;
  let responseSpeech;

  let requestQuery = Object.assign({}, config.graphqlRequestBody);
  if (
    req.body &&
    req.body.request &&
    req.body.request.intent &&
    req.body.request.intent.name
  ) {
    intent = req.body.request.intent.name;
    requestClient = "alexa";
  }
  if (
    req.body &&
    req.body.result &&
    req.body.result.metadata &&
    req.body.result.metadata.intentName
  ) {
    resolvedQuery = req.body.result.resolvedQuery || null;
    intent = req.body.result.metadata.intentName;
  }

  if (!intent) {
    return res.send({ error: "No Intent found!" });
  }

  /*
  //call graphql
  args = `intent:"${intent}"`;
  const rest = new Rest();
  let url = config.baseUrl + "/graphql?";
  let headers = config.header;
  requestQuery.query = requestQuery.query.replace("$args", args);
  let result = await rest.post(url, headers, requestQuery);
  */

  //call elastic search

  let rest = new Rest();
  let url =
    config.elasticSearchUrl +
    "/" +
    config.elasticSearchIndexName +
    "/chat/_search";
  let headers = config.header;
  let body = {
    query: {
      query_string: {
        query: intent,
        fields: ["intent"]
      }
    }
  };

  let result = await rest.post(url, headers, body);
  let tmpResult;

  if (result && result.hits && result.hits.hits[0]) {
    tmpResult = result.hits.hits[0]._source;
    tmpResult.id = result.hits.hits[0]._id;
  }

  result = { data: { chat: [] } };
  result.data.chat.push(tmpResult);

  /********************************/

  if (result && result.data && result.data.chat && result.data.chat[0]) {
    responseSpeech = result.data.chat[0].response.speech;
  }

  if (requestClient === "alexa" && responseSpeech) {
    ssmlSpeech = speech.say(responseSpeech).pause("200ms");
    console.log("ssmlSpeech", ssmlSpeech);
    finalResponse = {
      version: "1.0",
      response: {
        outputSpeech: {
          ssml: ssmlSpeech.ssml(),
          type: "SSML"
        },
        speechletResponse: {
          outputSpeech: {
            ssml: ssmlSpeech.ssml()
          }
        }
      },
      sessionAttributes: {}
    };
  } else {
    if (responseSpeech) {
      ssmlSpeech = speech.say(responseSpeech).pause("200ms");
    } else {
      ssmlSpeech = speech.say("Sorry, what was that?").pause("200ms");
    }

    finalResponse = {
      speech: "",
      displayText: "",
      data: {
        expectUserResponse: false
      },
      messages: [
        {
          type: 0,
          speech: ssmlSpeech.ssml()
        }
      ]
    };
  }
  res.send(finalResponse);
});

const server = app.listen(process.env.PORT, () => {
  const { port } = server.address();
  console.info(`\n\nExpress listen at http://localhost:${port} \n`);
  console.log("Exposed URL: ", config.baseUrl);
});
