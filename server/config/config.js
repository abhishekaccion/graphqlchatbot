process.env.NODE_ENV = process.env.NODE_ENV || "local";
process.env.PORT = process.env.PORT || 3002;
process.env.HOST = process.env.HOST || "127.0.0.1";
let intent;
let config = {
  baseUrl: "http://aicstage.accionlabs.com/chatservice",
  elasticSearchUrl: "http://localhost:9200",
  elasticSearchIndexName: "chatbot",
  graphqlRequestBody: {
    query: `{ chat($args) {    id,    response {      speech     data   }   type  }}`,
    variables: null,
    operationName: null
  },
  header: { "Content-Type": "application/json" }
};

export default config;
